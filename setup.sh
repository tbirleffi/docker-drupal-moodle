#!/bin/bash

ENV="$1";
PRJ="$2";
DIR1="data/www/drupal";
DIR2="data/www/moodle";
CUR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd );
COP="docker-compose.yml";
NAME1="drupal.com";
NAME2="moodle.com";
source ~/.bash_profile;

runDockerComposeSetup()
{
	if [ "$ENV" = "" ]
	then
		echo "Please provide the environment: local, dev, stage, prod, jenkins";
		exit 1;
	fi

	if [[ "$PRJ" = "" && "$ENV" != "local" ]]
	then
		echo "Please provide the environment project name: (ex: MyProjDev)";
		exit 1;
	fi

	if [ "$ENV" != "" ]
	then
		copyFiles;
		copyDockerComposeFile;
		startServices;
		listServices;
	fi
}

listServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" ps;
	else
		docker-compose ps;
	fi
}

startServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" up -d;
	else
		docker-compose up -d;
	fi
}

copyDockerComposeFile()
{
	cp "env/$ENV/docker-compose.yml" "$COP";
}

copyFiles()
{
	if [ -f "$DIR1/sites/default/settings.php" ]
	then
		rm -f "$DIR1/sites/default/settings.php";
	fi

	if [ -f "$DIR2/config.php" ]
	then
		rm -f "$DIR2/config.php";
	fi
	
	cp "settings.php" "$DIR1/sites/default/settings.php";
	cp "settings.php" "$DIR2/config.php";

	chmod a+w "$DIR1/sites/default/settings.php";
	chmod a+w "$DIR2/config.php";
}

createFolders()
{
	mkdir -p data/logs;
	mkdir -p data/www/moodledata;
}

runLocalNetworkSetup()
{
	echo Starting boot2docker if not already started;
	boot2docker status || boot2docker up;

	export the_ip=$(boot2docker ip 2>&1 | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
	export old_route_ip=$(netstat -r | grep 172.17  | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
	echo Found boot2docker ip $the_ip;

	if [ "$the_ip" == "" ]; then
		echo Could not find the ip of boot2docker.. =/;
		exit;
	fi

	echo Seting up routes. Enter sudo password;
	if [ "$old_route_ip" != "" ]; then
		sudo route -n delete -net 172.17.0.0 $old_route_ip;
	fi
	sudo route -n add -net 172.17.0.0 $the_ip;

	echo Setting up hosts;
	cp /etc/hosts hosts;
	grep -v '# boot2dockerscriptdrupalmoodle' hosts > hosts_temp;
	mv hosts_temp hosts;
	for containerid in $(docker ps -q); do
		export domain=$(docker inspect $containerid | grep '"Domainname":' | cut -f2 -d":" | cut -f2 -d'"');
		export host=$(docker inspect $containerid | grep '"Hostname":' | cut -f2 -d":" | cut -f2 -d'"');
		export ip=$(docker inspect $containerid | grep 'IPAdd' | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
		
		if [[ "$domain" != "" && $domain == *"$NAME1"* && "$ENV" != "jenkins" ]]; then
			env=`echo $domain| cut -d '.' -f 1`;
			rm -f "$DIR1/sites/default/$domain.php";
			cp "env/$env/drupal/$env.php" "$DIR1/sites/default/$domain.php";

			echo Storing values $ip $host.$domain;
			echo $ip $host $domain \# boot2dockerscriptdrupalmoodle >> hosts;
		fi

		if [[ "$domain" != "" && $domain == *"$NAME2"* && "$ENV" != "jenkins" ]]; then
			env=`echo $domain| cut -d '.' -f 1`;
			rm -f "$DIR2/$domain.php"
			cp "env/$env/moodle/$env.php" "$DIR2/$domain.php";

			echo Storing values $ip $host.$domain;
			echo $ip $host $domain \# boot2dockerscriptdrupalmoodle >> hosts;
		fi
	done
	sudo mv hosts /etc/hosts;
}

getDrupal()
{
	chmod a+w "$DIR1/sites";
	chmod a+w "$DIR1/sites/default";
	if [ ! -f "$DIR1/index.php" ]
	then
		cd data/www/drupal;
		curl -O "http://ftp.drupal.org/files/projects/drupal-7.35.tar.gz";
		tar -xzvf drupal-7.35.tar.gz;
		rm drupal-7.35.tar.gz;
		mv drupal-7.35/* drupal-7.35/.htaccess ./;
		mv drupal-7.35/.gitignore ./;
		rmdir drupal-7.35;
		cd ../../../;
	fi
}

getMoodle()
{
	if [ ! -d "$DIR2" ]
	then
		cd data/www/;
		git clone --depth=1 -b MOODLE_28_STABLE --single-branch git://git.moodle.org/moodle.git;
		cd ../../;
	fi
}

createFolders;
getDrupal;
getMoodle;
runDockerComposeSetup;
runLocalNetworkSetup;
