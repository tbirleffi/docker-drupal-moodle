# Drupal/Moodle - Single SignOn Application Prototype

---

Stack: PHP-FPM, Nginx, MySQL, Drupal 7.34, Moodle 2.8. 
This application can rapidly create the setup to recreate a Drupal Website in sync with a Moodle website.

Modules/Plugins:
https://github.com/cannod/moodle­drupalservices/tree/2.x­2.x­dev
https://www.drupal.org/project/moodle_sso

## Mac OS X: Local Development

1. Start with a Mac
2. Install VirtualBox v4.3.20-96996, or higher:
```
http://download.virtualbox.org/virtualbox/4.3.20/VirtualBox-4.3.20-96996-OSX.dmg
```
3. Install Docker and Boot2Docker, that comes with the install: 
```
https://docs.docker.com/installation/mac
```
4. Get Docker Compose, NOTE: Always check here, https://github.com/docker/fig/releases, for the latest release:
```
curl -L https://github.com/docker/compose/releases/download/1.1.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```
5. CREATE A FOLDER HERE: /Users/Sites
6. THIS PROJECT MUST BE CHECKED OUT HERE: /Users/Sites/{project-folder-name}
7. Open up a terminal, and cd /Users/Sites/{project-folder-name}
8. Run: boot2docker init
9. Run: boot2docker up
11. NOTE: Don't know the ip, just type: boot2docker ip
12. Open: /Users/{user}/.bash_profile, and paste:
```
export DOCKER_HOST=tcp://$(boot2docker ip 2>/dev/null):2376
export DOCKER_CERT_PATH=/Users/{user}/.boot2docker/certs/boot2docker-vm
export DOCKER_TLS_VERIFY=1
```
13. Restart terminal, and cd /Users/Sites/{project-folder-name}
14. Run the profile fix: ./profile-fix.sh
15. Run: ./setup.sh local
16. Note: If you sometimes get this error:
```
Couldn't connect to Docker daemon - you might need to run `boot2docker up`.
```
17. Just run: ./setup.sh local again
18. Browse to: {generated_ip}, and you should see something!

### MySQL: Local Connection

1. MySQL Host: 127.0.0.1
2. Username: root
3. Password: root
4. Database: {database_name}
5. Port: 3306
6. SSH Host: {generated_ip}
7. SSH User: docker
8. SSH Password: tcuser
9. SSH Port: 22

### Composer Commands: Run As Needed
Note: About the composer service container, it sets it up with SSL/TLS off, you need to setup composer with an actual certificate on your server when you run this container on an actual live environment:
```
curl -sS https://getcomposer.org/installer | php -- --disable-tls
All settings correct for using Composer
You have instructed the Installer not to enforce SSL/TLS security on remote HTTPS requests.
This will leave all downloads during installation vulnerable to Man-In-The-Middle (MITM) attacks.
```
1. docker-compose run composer self-update
2. docker-compose run composer install

### Setup Scripts: Per Environment

1. ./setup local
2. ./setup dev {project_name}
3. ./setup stage {project_name}
4. ./setup prod {project_name}
5. ./setup jenkins {project_name}

### Teardown Scripts: Per Environment

1. ./teardown local
2. ./teardown dev {project_name}
3. ./teardown stage {project_name}
4. ./teardown prod {project_name}
5. ./teardown jenkins {project_name}

### Local Host File: Config
1. {generated_ip} local.{domain_name}.com
2. {generated_ip} dev.{domain_name}.com
3. {generated_ip} stage.{domain_name}.com
4. {generated_ip} {domain_name}.com
5. {generated_ip} jenkins.{domain_name}.com:8080

